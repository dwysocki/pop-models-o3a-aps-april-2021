# PopModels O3a Results for APS April Meeting 2021

This repo contains several population inference runs using the PopModels population inference code [[GitLab](https://tinyurl.com/PopModels)] [[arXiv:1805.06442](https://arxiv.org/abs/1805.06442)] using open LIGO/Virgo data from GWTC-2 [[arXiv:2010.14527](https://arxiv.org/abs/2010.14527)].  These results were presented at the 2021 APS April Meeting, and will appear in an upcoming paper, Wysocki & O'Shaughnessy (2021).


## Try it yourself

This repository contains everything you need to reproduce the results it contains.  You are encouraged to use it to verify the results, experiment with the data products, and use it as a tutorial for population inference with PopModels.

First you should set up a virtual environment in the `venv/` directory as described by its [README](venv/README.md).  Then you'll want to download the GWTC-2 data into the `data/hdf5/` directory as described by its [README](data/hdf5/README.md).  That data should then be preprocessed into the text files required by PopModels according to the [README](data/text/README.md) in `data/text/`.  You'll then have everything you need to reproduce any of the results in the `runs/` directory.

Once your runs are finished, or just using the runs already included in this repository, you can reproduce all of the figures that appear in the talk with scripts in the `bin/` directory (their names all start with `plot`).  Note that some of the runs in this repository were not included in the talk because their MCMCs weren't finished running in time, so take those results with a grain of salt.

## Results

### MultiSpin model

#### Description

![MultiSpin cartoon](img/cartoons/multi-spin.png)

MultiSpin is a joint mass-spin model for binary black holes, introduced in the LVK O3a population paper [[arXiv:2010.14533](https://arxiv.org/abs/2010.14533)].  Independent analyses have shown that there is a feature in the BBH mass spectrum around 30-40 solar masses, which can be approximated by a Gaussian peak added on top of a power law continuum.  This model allows the power law and Gaussian components to have independent spin models.  This will in principle allow us to determine if the black hole spin spectrum is different in this 30-40 solar mass feature.  As we'll see below, with the current available data, the spin distribution is consistent with no change across the mass 1-mass 2 plane, and more data will be needed to measure a discrepancy.

#### Merger rates

![MultiSpin rates](img/MultiSpin_rates.png)

#### Mass distribution

![MultiSpin mass distribution legend](img/MultiSpin_m1_traces_legend.png)

![MultiSpin overall mass distribution](img/MultiSpin_m1_traces.png)
![MultiSpin mass distribution by subpopulation](img/MultiSpin_m1_traces_by_subpop.png)

#### Measuring spins

![MultiSpin mass distribution legend](img/MultiSpin_m1_traces_legend.png)

![MultiSpin overall mass distribution](img/MultiSpin_spin_traces.png)
![MultiSpin mass distribution by subpopulation](img/MultiSpin_spin_traces_by_subpop.png)


![MultiSpin hyperparameter posterior plot](img/MultiSpin_spin_hyperparameters.png)


### MultiSource model

#### Description

![MultiSource cartoon](img/cartoons/multi-source.png)

MultiSource is a mixture model that simultaneously fits binary black holes, binary neutron stars, and neutron star-black hole binaries.  The subpopulation that fits binary black holes is just MultiSpin.  Then binary neutron stars and neutron star black holes each get their own Gaussian subpopulations.  To keep things from getting too complicated, we assume that all neutron star masses are drawn from a Gaussian with the same location and width, and also have the same spin distribution.  The black holes in neutron star-black hole binaries come from one additional Gaussian mass distribution.  We also allow the model to fit for a maximum neutron star mass which is identical across all subpopulations, as one would expect from an equation of state-induced maximum mass.  As you will see in the mass distribution plots below, the models favor a mass gap between black holes and neutron stars.  However, for the supported gaps to make sense, GW190814 [[arXiv:2006.12611](https://arxiv.org/abs/2006.12611)] must be a neutron star-black hole binary, rather than a binary black hole.

#### Merger rates

![MultiSource rates](img/MultiSource_rates.png)

#### Mass distribution

![MultiSource mass distribution legend](img/MultiSource_m1_traces_legend.png)

![MultiSource overall mass distribution](img/MultiSource_m1_traces.png)
![MultiSource mass distribution by subpopulation](img/MultiSource_m1_traces_by_subpop.png)

Here's zoomed in on 2 to 10 solar masses to highlight support for a mass gap.

![MultiSource mass distribution legend](img/MultiSource_m1_traces_legend.png)

![MultiSource mass gap](img/MultiSource_mass_gap.png)
![MultiSource mass gap by subpopulation](img/MultiSource_mass_gap_by_subpop.png)

#### Measuring spins

![MultiSource BH spin hyperparameter posterior plot](img/MultiSource_BH_spin_hyperparameters.png)


## Credit

PopModels was developed by Daniel Wysocki and Richard O'Shaughnessy.

The runs in this repository were carried out by Daniel Wysocki.

Other software used includes LALSuite, much of the scientific Python stack (NumPy, SciPy, Matplotlib and H5Py), AstroPy, and Seaborn.

Parts of this work were carried out under funding from the NSF (PHY-1912649, PHY-1707965, AST-1909534) and RIT through the FGWA SIRA initiative. Computational resources provided by the LIGO Laboratory and supported by NSF (PHY-0757058, PHY-0823459)

This material is based upon work supported by NSF’s LIGO Laboratory which is a major facility fully funded by the National Science Foundation.

The figures in this talk were made with the help of Seaborne's `colorblind` color sequence, listed below:

`#0173B2` `#DE8F05` `#029E73` `#D55E00` `#CC78BC` `#CA9161` `#FBAFE4` `#949494` `#ECE133` `#56B4E9`
