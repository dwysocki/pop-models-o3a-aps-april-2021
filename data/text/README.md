# GWTC-2 text files

This is where the text version of the GWTC-2 data is stored.  After fetching the HDF5 data (see [`data/hdf5/README.md`](data/hdf5/README.md)), you can generate this with `/bin/convert_hdf5_to_text.py`.
