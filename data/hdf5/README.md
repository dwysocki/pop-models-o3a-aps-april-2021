# GWTC-2 HDF5 files

This is where the HDF5 files for GWTC-2 open data are stored.  You will need to run `/bin/fetch_and_plot_gwtc2.sh` to get a local copy.
