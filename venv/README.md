# Virtual Environment

You should create a Python virtual environment with the necessary packages installed here.

You will need Python 3.6, 3.7, or 3.8 to run everything successfully.  We'll assume your python executable is called `python`, but it might be something else (e.g., `python3`, `python3.8`), so replace all of the `python` commands with whatever yours is called.  You will also want to have `pip` installed for your Python environment.  If you don't know if you have `pip`, you can either install it according to your operating system's instructions, or simply run
```
$ python -m ensurepip
```

Once `pip` is available, use it to install `virtualenv`
```
$ python -m pip install virtualenv
```

Now initialize a virtual environment in the `venv/` directory
```
$ python -m virtualenv .
```

Whenever you want to run this demo, make sure the virtual environment is activated for your session
```
$ source bin/activate
```

You can deactivate it at any time using the `deactivate` command, but don't do that yet.


Now we will download and install PopModels (for doing population inference) with the following
```
(venv) $ git clone https://git.ligo.org/daniel.wysocki/bayesian-parametric-population-models.git PopModels

(venv) $ cd PopModels
(venv) $ python -m pip install .
(venv) $ cd -
```

And we will download and install GWTC-explorer (for fetching and plotting data) with the following
```
(venv) $ git clone https://git.ligo.org/daniel.wysocki/gwtc-explorer.git

(venv) $ cd gwtc-explorer
(venv) $ python -m pip install .
(venv) $ cd -
```
