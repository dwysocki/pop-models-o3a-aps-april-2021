# Runs

This directory contains the individual configurations for different PopModels models.  The APS talk covered the MultiSpin and MultiSource models.  There are also several extensions of MultiSource which have not been run to convergence, so please take them with a grain of salt.

- [MultiSpin](./MultiSpin/)
- [MultiSource](./MultiSource/)
