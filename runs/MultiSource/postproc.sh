#!/bin/bash

# Plot the raw chains, skipping the first few samples to reduce the plot's
# dynamic range
pop_models_h5_raw_samples post_raw/ plot_chains chains.png --skip-samples 100

# Extract independent samples from MCMC chains automatically
# WARNING: will give bad results if chains are too short
pop_models_extract_samples post_raw/ post_cleaned.hdf5 \
    --burnin-plot burnin.png --acorr-plot acorr.png \
    --acorr-safety-factor 1 \
    --force

# Use an manually chosen burnin and autocorrelation length to extract samples
# from chains that have not converged enough for the automated method.
# NOTE: You can play with these numbers yourself.
pop_models_extract_samples post_raw/ post_cleaned_manual.hdf5 \
    --fixed-burnin 2500 --fixed-thinning 50 \
    --force

# Create 1-D corner plot
pop_models_overlay \
    corner_1d.png \
    --overplot-posteriors post_cleaned.hdf5 \
    --no-legend \
    --only-1d
pop_models_overlay \
    corner_1d_manual.png \
    --overplot-posteriors post_cleaned_manual.hdf5 \
    --no-legend \
    --only-1d

# Create PPD samples
pop_models_bbh_mixture_pl_gauss ppd \
    10000 post_cleaned.hdf5 ppd_samples.dat \
    --record-hyperparameters \
    --thin-stuck
pop_models_bbh_mixture_pl_gauss ppd \
    10000 post_cleaned_manual.hdf5 ppd_samples_manual.dat \
    --record-hyperparameters \
    --thin-stuck

# Create source-specific PPD samples
pop_models_bbh_mixture_pl_gauss ppd \
    10000 post_cleaned.hdf5 ppd_samples_BBH.dat \
    --isolate-subpops pl0 g0 \
    --record-hyperparameters \
    --thin-stuck
pop_models_bbh_mixture_pl_gauss ppd \
    10000 post_cleaned_manual.hdf5 ppd_samples_BBH_manual.dat \
    --isolate-subpops pl0 g0 \
    --record-hyperparameters \
    --thin-stuck

pop_models_bbh_mixture_pl_gauss ppd \
    10000 post_cleaned.hdf5 ppd_samples_BNS.dat \
    --isolate-subpops g2 \
    --record-hyperparameters \
    --thin-stuck
pop_models_bbh_mixture_pl_gauss ppd \
    10000 post_cleaned_manual.hdf5 ppd_samples_BNS_manual.dat \
    --isolate-subpops g2 \
    --record-hyperparameters \
    --thin-stuck

pop_models_bbh_mixture_pl_gauss ppd \
    10000 post_cleaned.hdf5 ppd_samples_NSBH.dat \
    --isolate-subpops g1 \
    --record-hyperparameters \
    --thin-stuck
pop_models_bbh_mixture_pl_gauss ppd \
    10000 post_cleaned_manual.hdf5 ppd_samples_NSBH_manual.dat \
    --isolate-subpops g1 \
    --record-hyperparameters \
    --thin-stuck


# Create mass and spin distribution plots
pop_models_bbh_mixture_pl_gauss plot_mass_distribution \
    post_cleaned.hdf5 \
    dist_Rpm1.png dist_pm1.png \
    dist_Rpq.png dist_pq.png \
    --output-files \
        dist_Rpm1.hdf5 dist_pm1.hdf5 \
        dist_Rpq.hdf5 dist_pq.hdf5 \
    --n-plot-points 1000 \
    --mass-range 1 100 \
    --m1-scale log --n-oom 4 \
    --skip-mass-ratio \
    --force &
pop_models_bbh_mixture_pl_gauss plot_mass_distribution \
    post_cleaned_manual.hdf5 \
    dist_Rpm1.png dist_pm1.png \
    dist_Rpq.png dist_pq.png \
    --output-files \
        dist_Rpm1_manual.hdf5 dist_pm1_manual.hdf5 \
        dist_Rpq_manual.hdf5 dist_pq_manual.hdf5 \
    --n-plot-points 1000 \
    --mass-range 1 100 \
    --m1-scale log --n-oom 4 \
    --skip-mass-ratio \
    --force &
pop_models_bbh_mixture_pl_gauss plot_mass_distribution \
    post_cleaned.hdf5 \
    dist_Rpm1_BBH.png dist_pm1_BBH.png \
    dist_Rpq_BBH.png dist_pq_BBH.png \
    --output-files \
        dist_Rpm1_BBH.hdf5 dist_pm1_BBH.hdf5 \
        dist_Rpq_BBH.hdf5 dist_pq_BBH.hdf5 \
    --n-plot-points 1000 \
    --mass-range 1 100 \
    --m1-scale log --n-oom 4 \
    --isolate-subpops pl0 g0 \
    --skip-mass-ratio \
    --force &
pop_models_bbh_mixture_pl_gauss plot_mass_distribution \
    post_cleaned_manual.hdf5 \
    dist_Rpm1_BBH_manual.png dist_pm1_BBH_manual.png \
    dist_Rpq_BBH_manual.png dist_pq_BBH_manual.png \
    --output-files \
        dist_Rpm1_BBH_manual.hdf5 dist_pm1_BBH_manual.hdf5 \
        dist_Rpq_BBH_manual.hdf5 dist_pq_BBH_manual.hdf5 \
    --n-plot-points 1000 \
    --mass-range 1 100 \
    --m1-scale log --n-oom 4 \
    --isolate-subpops pl0 g0 \
    --skip-mass-ratio \
    --force &
pop_models_bbh_mixture_pl_gauss plot_mass_distribution \
    post_cleaned.hdf5 \
    dist_Rpm1_BBH_pl.png dist_pm1_BBH_pl.png \
    dist_Rpq_BBH_pl.png dist_pq_BBH_pl.png \
    --output-files \
        dist_Rpm1_BBH_pl.hdf5 dist_pm1_BBH_pl.hdf5 \
        dist_Rpq_BBH_pl.hdf5 dist_pq_BBH_pl.hdf5 \
    --n-plot-points 1000 \
    --mass-range 1 100 \
    --m1-scale log --n-oom 4 \
    --isolate-subpops pl0 \
    --skip-mass-ratio \
    --force &
pop_models_bbh_mixture_pl_gauss plot_mass_distribution \
    post_cleaned_manual.hdf5 \
    dist_Rpm1_BBH_pl_manual.png dist_pm1_BBH_pl_manual.png \
    dist_Rpq_BBH_pl_manual.png dist_pq_BBH_pl_manual.png \
    --output-files \
        dist_Rpm1_BBH_pl_manual.hdf5 dist_pm1_BBH_pl_manual.hdf5 \
        dist_Rpq_BBH_pl_manual.hdf5 dist_pq_BBH_pl_manual.hdf5 \
    --n-plot-points 1000 \
    --mass-range 1 100 \
    --m1-scale log --n-oom 4 \
    --isolate-subpops pl0 \
    --skip-mass-ratio \
    --force &
pop_models_bbh_mixture_pl_gauss plot_mass_distribution \
    post_cleaned.hdf5 \
    dist_Rpm1_BBH_g.png dist_pm1_BBH_g.png \
    dist_Rpq_BBH_g.png dist_pq_BBH_g.png \
    --output-files \
        dist_Rpm1_BBH_g.hdf5 dist_pm1_BBH_g.hdf5 \
        dist_Rpq_BBH_g.hdf5 dist_pq_BBH_g.hdf5 \
    --n-plot-points 1000 \
    --mass-range 1 100 \
    --m1-scale log --n-oom 4 \
    --isolate-subpops g0 \
    --skip-mass-ratio \
    --force &
pop_models_bbh_mixture_pl_gauss plot_mass_distribution \
    post_cleaned_manual.hdf5 \
    dist_Rpm1_BBH_g_manual.png dist_pm1_BBH_g_manual.png \
    dist_Rpq_BBH_g_manual.png dist_pq_BBH_g_manual.png \
    --output-files \
        dist_Rpm1_BBH_g_manual.hdf5 dist_pm1_BBH_g_manual.hdf5 \
        dist_Rpq_BBH_g_manual.hdf5 dist_pq_BBH_g_manual.hdf5 \
    --n-plot-points 1000 \
    --mass-range 1 100 \
    --m1-scale log --n-oom 4 \
    --isolate-subpops g0 \
    --skip-mass-ratio \
    --force &
pop_models_bbh_mixture_pl_gauss plot_mass_distribution \
    post_cleaned.hdf5 \
    dist_Rpm1_NSBH.png dist_pm1_NSBH.png \
    dist_Rpq_NSBH.png dist_pq_NSBH.png \
    --output-files \
        dist_Rpm1_NSBH.hdf5 dist_pm1_NSBH.hdf5 \
        dist_Rpq_NSBH.hdf5 dist_pq_NSBH.hdf5 \
    --n-plot-points 1000 \
    --mass-range 1 100 \
    --m1-scale log --n-oom 4 \
    --isolate-subpops g1 \
    --skip-mass-ratio \
    --force &
pop_models_bbh_mixture_pl_gauss plot_mass_distribution \
    post_cleaned_manual.hdf5 \
    dist_Rpm1_NSBH_manual.png dist_pm1_NSBH_manual.png \
    dist_Rpq_NSBH_manual.png dist_pq_NSBH_manual.png \
    --output-files \
        dist_Rpm1_NSBH_manual.hdf5 dist_pm1_NSBH_manual.hdf5 \
        dist_Rpq_NSBH_manual.hdf5 dist_pq_NSBH_manual.hdf5 \
    --n-plot-points 1000 \
    --mass-range 1 100 \
    --m1-scale log --n-oom 4 \
    --isolate-subpops g1 \
    --skip-mass-ratio \
    --force &
pop_models_bbh_mixture_pl_gauss plot_mass_distribution \
    post_cleaned.hdf5 \
    dist_Rpm1_BNS.png dist_pm1_BNS.png \
    dist_Rpq_BNS.png dist_pq_BNS.png \
    --output-files \
        dist_Rpm1_BNS.hdf5 dist_pm1_BNS.hdf5 \
        dist_Rpq_BNS.hdf5 dist_pq_BNS.hdf5 \
    --n-plot-points 1000 \
    --mass-range 1 100 \
    --m1-scale log --n-oom 4 \
    --isolate-subpops g2 \
    --skip-mass-ratio \
    --force &
pop_models_bbh_mixture_pl_gauss plot_mass_distribution \
    post_cleaned_manual.hdf5 \
    dist_Rpm1_BNS_manual.png dist_pm1_BNS_manual.png \
    dist_Rpq_BNS_manual.png dist_pq_BNS_manual.png \
    --output-files \
        dist_Rpm1_BNS_manual.hdf5 dist_pm1_BNS_manual.hdf5 \
        dist_Rpq_BNS_manual.hdf5 dist_pq_BNS_manual.hdf5 \
    --n-plot-points 1000 \
    --mass-range 1 100 \
    --m1-scale log --n-oom 4 \
    --isolate-subpops g2 \
    --skip-mass-ratio \
    --force &

pop_models_bbh_mixture_pl_gauss plot_spin_distribution \
    post_cleaned.hdf5 \
    dist_Rpchi.png dist_pchi.png \
    dist_Rpcostilt.png dist_pcostilt.png \
    --output-files \
        dist_Rpchi1.hdf5 dist_pchi1.hdf5 \
        dist_Rpchi2.hdf5 dist_pchi2.hdf5 \
        dist_Rpcostilt1.hdf5 dist_pcostilt1.hdf5 \
        dist_Rpcostilt2.hdf5 dist_pcostilt2.hdf5 \
    --n-oom 4 \
    --force &
pop_models_bbh_mixture_pl_gauss plot_spin_distribution \
    post_cleaned.hdf5 \
    dist_Rpchi_pl0.png dist_pchi_pl0.png \
    dist_Rpcostilt_pl0.png dist_pcostilt_pl0.png \
    --output-files \
        dist_Rpchi1_pl0.hdf5 dist_pchi1_pl0.hdf5 \
        dist_Rpchi2_pl0.hdf5 dist_pchi2_pl0.hdf5 \
        dist_Rpcostilt1_pl0.hdf5 dist_pcostilt1_pl0.hdf5 \
        dist_Rpcostilt2_pl0.hdf5 dist_pcostilt2_pl0.hdf5 \
    --n-oom 4 \
    --isolate-subpops pl0 \
    --force &
pop_models_bbh_mixture_pl_gauss plot_spin_distribution \
    post_cleaned.hdf5 \
    dist_Rpchi_g0.png dist_pchi_g0.png \
    dist_Rpcostilt_g0.png dist_pcostilt_g0.png \
    --output-files \
        dist_Rpchi1_g0.hdf5 dist_pchi1_g0.hdf5 \
        dist_Rpchi2_g0.hdf5 dist_pchi2_g0.hdf5 \
        dist_Rpcostilt1_g0.hdf5 dist_pcostilt1_g0.hdf5 \
        dist_Rpcostilt2_g0.hdf5 dist_pcostilt2_g0.hdf5 \
    --n-oom 4 \
    --isolate-subpops g0 \
    --force &
