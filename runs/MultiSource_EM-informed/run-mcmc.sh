#!/bin/bash

pop_models_bbh_mixture_pl_gauss inference \
  samples/GW150914_GWTC-1.dat samples/GW151012_GWTC-1.dat samples/GW151226_GWTC-1.dat samples/GW170104_GWTC-1.dat samples/GW170608_GWTC-1.dat samples/GW170729_GWTC-1.dat samples/GW170809_GWTC-1.dat samples/GW170814_GWTC-1.dat samples/GW170817_GWTC-1.dat samples/GW170818_GWTC-1.dat samples/GW170823_GWTC-1.dat samples/GW190408_181802.dat samples/GW190412.dat samples/GW190413_052954.dat samples/GW190413_134308.dat samples/GW190421_213856.dat samples/GW190424_180648.dat samples/GW190425.dat samples/GW190503_185404.dat samples/GW190512_180714.dat samples/GW190513_205428.dat samples/GW190514_065416.dat samples/GW190517_055101.dat samples/GW190519_153544.dat samples/GW190521_074359.dat samples/GW190521.dat samples/GW190527_092055.dat samples/GW190602_175927.dat samples/GW190620_030421.dat samples/GW190630_185205.dat samples/GW190701_203306.dat samples/GW190706_222641.dat samples/GW190707_093326.dat samples/GW190708_232457.dat samples/GW190720_000836.dat samples/GW190727_060333.dat samples/GW190728_064510.dat samples/GW190731_140936.dat samples/GW190803_022701.dat samples/GW190814.dat samples/GW190828_063405.dat samples/GW190828_065509.dat samples/GW190910_112807.dat samples/GW190915_235702.dat samples/GW190924_021846.dat samples/GW190929_012149.dat samples/GW190930_133541.dat \
  vt.hdf5 \
  prior.json \
  post_raw/ \
  --constants constants.json --duplicates duplicates.json \
  --spin-scalings spin_scales.json \
  --moves moves.json \
  --rescale-params rescale_params.json \
  --vt-calibration calibration.json \
  --n-samples 5000 --n-walkers 256 \
  --n-threads 1 \
  --n-powerlaws 1 --n-gaussians 3 --total-mass-max 300 --no-spin-singularities \
  --verbose \
  >> logs/run-mcmc.out \
  2>> logs/run-mcmc.err
