#!/bin/bash

# Downloads EM constraints on neutron stars from Farr and Chatziioannou (2020)
# doi:10.3847/2515-5172/ab9088
# Renames file to be more descriptive
wget -O NS_EM_constraints.h5 https://github.com/farr/AlsingNSMassReplication/raw/16b3b3eb96c46650e3e48c5c1d130ab6c1b5805d/ar.nc
