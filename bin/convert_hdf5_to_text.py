import warnings
import traceback

import os
import sys
import glob

import numpy as np

from pesummary.io import read as pesummary_read
#from pesummary.gw.file.read import read as GWread

seed = 99
random_state = np.random.RandomState(seed)

n_samples_to_keep = 4000

params_to_save = ("m1_source", "m2_source", "a1", "a2", "costilt1", "costilt2")
param_renaming = {
    "m1_source" : "mass_1_source",
    "m2_source" : "mass_2_source",
    "a1" : "a_1",
    "a2" : "a_2",
    "costilt1" : "cos_tilt_1",
    "costilt2" : "cos_tilt_2",
}
txt_header = " ".join(params_to_save)

def save_samples_dict(samples_dict, fname):
    saved_arr = np.column_stack(tuple(
        samples_dict[param_renaming[param_name]]
        for param_name in params_to_save
    ))

    # Truncate samples to a fixed upper limit, shuffling first.
    if len(saved_arr) > n_samples_to_keep:
        random_state.shuffle(saved_arr)
        saved_arr = saved_arr[:n_samples_to_keep]

    np.savetxt(fname, saved_arr, header=txt_header)



def convert_GWTC1(h5_fname, txt_fname):
    try:
        dat = pesummary_read(h5_fname)
    except:
        # Handle special case for GW170817
        dat = pesummary_read(
            h5_fname,
            path_to_samples="IMRPhenomPv2NRT_lowSpin_posterior",
        )

    dat.generate_all_posterior_samples()

    samples_arr = np.asarray(dat.samples)
    samples_dict = dict(zip(dat.parameters, samples_arr.T))

    save_samples_dict(samples_dict, txt_fname)


def convert_GWTC2(h5_fname, txt_fname):
    dat = pesummary_read(h5_fname)

    analysis_label = dat.labels.index("PublicationSamples")

    # Extract posterior samples as dict.
    samples_arr = np.asarray(dat.samples[analysis_label])
    samples_dict = dict(zip(dat.parameters[analysis_label], samples_arr.T))

    save_samples_dict(samples_dict, txt_fname)


def convert(h5_fname, txt_fname):
    try:
        return convert_GWTC2(h5_fname, txt_fname)
    except:
        return convert_GWTC1(h5_fname, txt_fname)


# Iterate over all HDF5 files.
extensions = [".hdf5", ".h5", ".hdf"]
for event_path in glob.glob("../data/hdf5/GW*"):
    # Get event name without any extension.
    event_name = os.path.basename(event_path).split(".")[0]

    # Some events have a single HDF5 file, while others are in a subdirectory.
    # Get the name of the HDF5 file to load.
    if any(event_path.endswith(e) for e in extensions):
        h5_fname = event_path
    else:
        h5_fname = os.path.join(event_path, event_name+".h5")

    txt_fname = os.path.join("../data/text/", event_name+".dat")

    print("Converting", event_name)
    try:
        convert(h5_fname, txt_fname)
    except Exception as e:
        warnings.warn("Failed to convert {}".format(h5_fname))
        traceback.print_exc(file=sys.stderr)
