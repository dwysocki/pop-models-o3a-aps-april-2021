#!/bin/bash
#
# Uses GWTC-explorer to download GWTC-2 and make a simple plot.
#

gwtc_explorer scatter_2d \
    --save-fig ../img/GWTC2_scatter_m1m2.png \
    --catalog-min 1 --catalog-max 2 \
    --event-dir ../data/hdf5/ \
    --parameters mass_1_source mass_2_source
