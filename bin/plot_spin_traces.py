import numpy as np
import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from pop_models.credible_regions import CredibleRegions1D
from pop_models.utils.plotting import limit_ax_oom

# Limit number of OoM in distribution plots so log-log scale doesn't kill us.
n_oom = 4

# Colors for subpopulations (pl and g)
color_bbh_pl = "#DE8F05"
color_bbh_g = "#0173B2"
color_bbh = "#029E73"
color_bns = "#D55E00"
color_nsbh = "#CC78BC"
color_total = "#56B4E9"

# Linestyles for whether or not GW190426 is included
linestyle_default = "solid"
linestyle_190426 = "dashed"


## MultiSpin results ###########################################################

# Load in credible regions data
crs_fname = "../runs/MultiSpin/dist_pchi1_pl0.hdf5"
crs_chi1_bbh_pl = CredibleRegions1D.load(crs_fname)
crs_fname = "../runs/MultiSpin/dist_pchi2_pl0.hdf5"
crs_chi2_bbh_pl = CredibleRegions1D.load(crs_fname)
crs_fname = "../runs/MultiSpin/dist_pcostilt1_pl0.hdf5"
crs_costilt1_bbh_pl = CredibleRegions1D.load(crs_fname)
crs_fname = "../runs/MultiSpin/dist_pcostilt2_pl0.hdf5"
crs_costilt2_bbh_pl = CredibleRegions1D.load(crs_fname)
crs_fname = "../runs/MultiSpin/dist_pchi1_g0.hdf5"
crs_chi1_bbh_g = CredibleRegions1D.load(crs_fname)
crs_fname = "../runs/MultiSpin/dist_pchi2_g0.hdf5"
crs_chi2_bbh_g = CredibleRegions1D.load(crs_fname)
crs_fname = "../runs/MultiSpin/dist_pcostilt1_g0.hdf5"
crs_costilt1_bbh_g = CredibleRegions1D.load(crs_fname)
crs_fname = "../runs/MultiSpin/dist_pcostilt2_g0.hdf5"
crs_costilt2_bbh_g = CredibleRegions1D.load(crs_fname)
crs_fname = "../runs/MultiSpin/dist_pchi1.hdf5"
crs_chi1_bbh = CredibleRegions1D.load(crs_fname)
crs_fname = "../runs/MultiSpin/dist_pchi2.hdf5"
crs_chi2_bbh = CredibleRegions1D.load(crs_fname)
crs_fname = "../runs/MultiSpin/dist_pcostilt1.hdf5"
crs_costilt1_bbh = CredibleRegions1D.load(crs_fname)
crs_fname = "../runs/MultiSpin/dist_pcostilt2.hdf5"
crs_costilt2_bbh = CredibleRegions1D.load(crs_fname)

# Plot traces for total population and individual subpopulations on separate
# files.  To ensure the two line up exactly, we plot everything on both figures,
# but make some elements transparent.

# Plot overall distribution
fig, axes = plt.subplots(
    2, 2,
    sharex="col",
    figsize=(6,8),
    constrained_layout=True,
)
(ax_chi1, ax_cost1), (ax_chi2, ax_cost2) = axes
axes_chi = ax_chi1, ax_chi2
axes_cost = ax_cost1, ax_cost2

# Plot traces.
ax_chi1.loglog(
    crs_chi1_bbh.indep_variable, crs_chi1_bbh.curves.T,
    color=color_bbh, alpha=0.05,
)
ax_chi2.loglog(
    crs_chi2_bbh.indep_variable, crs_chi2_bbh.curves.T,
    color=color_bbh, alpha=0.05,
)
ax_cost1.plot(
    crs_costilt1_bbh.indep_variable, crs_costilt1_bbh.curves.T,
    color=color_bbh, alpha=0.05,
)
ax_cost2.plot(
    crs_costilt2_bbh.indep_variable, crs_costilt2_bbh.curves.T,
    color=color_bbh, alpha=0.05,
)

# Plot credible intervals
ax_chi1.loglog(
    crs_chi1_bbh.indep_variable,
    np.transpose(crs_chi1_bbh.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)
ax_chi2.loglog(
    crs_chi2_bbh.indep_variable,
    np.transpose(crs_chi2_bbh.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)
ax_cost1.plot(
    crs_costilt1_bbh.indep_variable,
    np.transpose(crs_costilt1_bbh.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)
ax_cost2.plot(
    crs_costilt2_bbh.indep_variable,
    np.transpose(crs_costilt2_bbh.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)

# Plot posterior predictive distribution
ax_chi1.loglog(
    crs_chi1_bbh.indep_variable, crs_chi1_bbh.mean(),
    color="black", linestyle="solid", lw=1,
)
ax_chi2.loglog(
    crs_chi2_bbh.indep_variable, crs_chi2_bbh.mean(),
    color="black", linestyle="solid", lw=1,
)
ax_cost1.plot(
    crs_costilt1_bbh.indep_variable, crs_costilt1_bbh.mean(),
    color="black", linestyle="solid", lw=1,
)
ax_cost2.plot(
    crs_costilt2_bbh.indep_variable, crs_costilt2_bbh.mean(),
    color="black", linestyle="solid", lw=1,
)

# Label axes
ax_chi2.set_xlabel("Magnitude", fontsize=14)
ax_cost2.set_xlabel("Cos(Tilt)", fontsize=14)
ax_chi1.set_ylabel("Distribution (primary)", fontsize=14)
ax_chi2.set_ylabel("Distribution (secondary)", fontsize=14)

# Limit the number of orders of magnitude for readability
for ax in axes_chi:
    limit_ax_oom(ax, n_oom, "y")

# Save to file
fig.savefig("../img/MultiSpin_spin_traces.png")

# Plot individual subpopulations
fig, axes = plt.subplots(
    2, 2,
    sharex="col",
    figsize=(6,8),
    constrained_layout=True,
)
(ax_chi1, ax_cost1), (ax_chi2, ax_cost2) = axes
axes_chi = ax_chi1, ax_chi2
axes_cost = ax_cost1, ax_cost2

# Plot traces.
ax_chi1.loglog(
    crs_chi1_bbh_pl.indep_variable, crs_chi1_bbh_pl.curves.T,
    color=color_bbh_pl, alpha=0.05,
)
ax_chi1.loglog(
    crs_chi1_bbh_g.indep_variable, crs_chi1_bbh_g.curves.T,
    color=color_bbh_g, alpha=0.05,
)

ax_chi2.loglog(
    crs_chi2_bbh_pl.indep_variable, crs_chi2_bbh_pl.curves.T,
    color=color_bbh_pl, alpha=0.05,
)
ax_chi2.loglog(
    crs_chi2_bbh_g.indep_variable, crs_chi2_bbh_g.curves.T,
    color=color_bbh_g, alpha=0.05,
)

ax_cost1.plot(
    crs_costilt1_bbh_pl.indep_variable, crs_costilt1_bbh_pl.curves.T,
    color=color_bbh_pl, alpha=0.05,
)
ax_cost1.plot(
    crs_costilt1_bbh_g.indep_variable, crs_costilt1_bbh_g.curves.T,
    color=color_bbh_g, alpha=0.05,
)

ax_cost2.plot(
    crs_costilt2_bbh_pl.indep_variable, crs_costilt2_bbh_pl.curves.T,
    color=color_bbh_pl, alpha=0.05,
)
ax_cost2.plot(
    crs_costilt2_bbh_g.indep_variable, crs_costilt2_bbh_g.curves.T,
    color=color_bbh_g, alpha=0.05,
)

# Plot credible intervals
ax_chi1.loglog(
    crs_chi1_bbh_pl.indep_variable,
    np.transpose(crs_chi1_bbh_pl.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)
ax_chi1.loglog(
    crs_chi1_bbh_g.indep_variable,
    np.transpose(crs_chi1_bbh_g.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)

ax_chi2.loglog(
    crs_chi2_bbh_pl.indep_variable,
    np.transpose(crs_chi2_bbh_pl.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)
ax_chi2.loglog(
    crs_chi2_bbh_g.indep_variable,
    np.transpose(crs_chi2_bbh_g.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)

ax_cost1.plot(
    crs_costilt1_bbh_pl.indep_variable,
    np.transpose(crs_costilt1_bbh_pl.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)
ax_cost1.plot(
    crs_costilt1_bbh_g.indep_variable,
    np.transpose(crs_costilt1_bbh_g.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)

ax_cost2.plot(
    crs_costilt2_bbh_pl.indep_variable,
    np.transpose(crs_costilt2_bbh_pl.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)
ax_cost2.plot(
    crs_costilt2_bbh_g.indep_variable,
    np.transpose(crs_costilt2_bbh_g.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)

# Plot posterior predictive distribution
ax_chi1.loglog(
    crs_chi1_bbh_pl.indep_variable, crs_chi1_bbh_pl.mean(),
    color="black", linestyle="solid", lw=1,
)
ax_chi1.loglog(
    crs_chi1_bbh_g.indep_variable, crs_chi1_bbh_g.mean(),
    color="black", linestyle="solid", lw=1,
)

ax_chi2.loglog(
    crs_chi2_bbh_pl.indep_variable, crs_chi2_bbh_pl.mean(),
    color="black", linestyle="solid", lw=1,
)
ax_chi2.loglog(
    crs_chi2_bbh_g.indep_variable, crs_chi2_bbh_g.mean(),
    color="black", linestyle="solid", lw=1,
)

ax_cost1.plot(
    crs_costilt1_bbh_pl.indep_variable, crs_costilt1_bbh_pl.mean(),
    color="black", linestyle="solid", lw=1,
)
ax_cost1.plot(
    crs_costilt1_bbh_g.indep_variable, crs_costilt1_bbh_g.mean(),
    color="black", linestyle="solid", lw=1,
)

ax_cost2.plot(
    crs_costilt2_bbh_pl.indep_variable, crs_costilt2_bbh_pl.mean(),
    color="black", linestyle="solid", lw=1,
)
ax_cost2.plot(
    crs_costilt2_bbh_g.indep_variable, crs_costilt2_bbh_g.mean(),
    color="black", linestyle="solid", lw=1,
)

# Label axes
ax_chi2.set_xlabel("Magnitude", fontsize=14)
ax_cost2.set_xlabel("Cos(Tilt)", fontsize=14)
ax_chi1.set_ylabel("Distribution (primary)", fontsize=14)
ax_chi2.set_ylabel("Distribution (secondary)", fontsize=14)

# Limit the number of orders of magnitude for readability
for ax in axes_chi:
    limit_ax_oom(ax, n_oom, "y")

# Save to file
fig.savefig("../img/MultiSpin_spin_traces_by_subpop.png")


import sys
sys.exit(0)

# Plot individual subpopulations
fig, ax = plt.subplots(figsize=(6,5), constrained_layout=True)

# Plot traces.
ax.loglog(
    crs_bbh.indep_variable, crs_bbh.curves.T,
    color=color_bbh, alpha=0,
)
ax.loglog(
    crs_bbh_pl.indep_variable, crs_bbh_pl.curves.T,
    color=color_bbh_pl, alpha=0.05,
)
ax.loglog(
    crs_bbh_g.indep_variable, crs_bbh_g.curves.T,
    color=color_bbh_g, alpha=0.05,
)

# Plot credible intervals
ax.loglog(
    crs_bbh.indep_variable, np.transpose(crs_bbh.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1, alpha=0,
)
ax.loglog(
    crs_bbh_pl.indep_variable, np.transpose(crs_bbh_pl.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)
ax.loglog(
    crs_bbh_g.indep_variable, np.transpose(crs_bbh_g.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)

# Plot posterior predictive distribution
ax.loglog(
    crs_bbh.indep_variable, crs_bbh.mean(),
    color="black", linestyle="solid", lw=1, alpha=0,
)
ax.loglog(
    crs_bbh_pl.indep_variable, crs_bbh_pl.mean(),
    color="black", linestyle="solid", lw=1,
)
ax.loglog(
    crs_bbh_g.indep_variable, crs_bbh_g.mean(),
    color="black", linestyle="solid", lw=1,
)

# Label axes
ax.set_xlabel("Primary mass [M$_\\odot$]", fontsize=14)
ax.set_ylabel("Mass spectrum", fontsize=14)

# Limit the number of orders of magnitude for readability
limit_ax_oom(ax, n_oom, "y")

# Save to file
fig.savefig("../img/MultiSpin_m1_traces_by_subpop.png")


## MultiSource results #########################################################

# Load in credible regions data
crs_fname = "../runs/MultiSource/dist_pm1_BBH.hdf5"
crs_bbh = CredibleRegions1D.load(crs_fname)
crs_fname = "../runs/MultiSource/dist_pm1_NSBH.hdf5"
crs_nsbh = CredibleRegions1D.load(crs_fname)
crs_fname = "../runs/MultiSource/dist_pm1_BNS.hdf5"
crs_bns = CredibleRegions1D.load(crs_fname)
crs_fname = "../runs/MultiSource/dist_pm1.hdf5"
crs_total = CredibleRegions1D.load(crs_fname)

# Plot traces for total population and individual subpopulations on separate
# files.  To ensure the two line up exactly, we plot everything on both figures,
# but make some elements transparent.

# Plot overall distribution
fig, ax = plt.subplots(figsize=(6,5), constrained_layout=True)

# Plot traces.
ax.loglog(
    crs_total.indep_variable, crs_total.curves.T,
    color=color_total, alpha=0.05,
)
ax.loglog(
    crs_bbh.indep_variable, crs_bbh.curves.T,
    color=color_bbh, alpha=0,
)
ax.loglog(
    crs_bns.indep_variable, crs_bns.curves.T,
    color=color_bns, alpha=0,
)
ax.loglog(
    crs_nsbh.indep_variable, crs_nsbh.curves.T,
    color=color_nsbh, alpha=0,
)

# Plot credible intervals
ax.loglog(
    crs_total.indep_variable, np.transpose(crs_total.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)
ax.loglog(
    crs_bbh.indep_variable, np.transpose(crs_bbh.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1, alpha=0,
)
ax.loglog(
    crs_bns.indep_variable, np.transpose(crs_bns.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1, alpha=0,
)
ax.loglog(
    crs_nsbh.indep_variable, np.transpose(crs_nsbh.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1, alpha=0,
)


# Plot posterior predictive distribution
ax.loglog(
    crs_total.indep_variable, crs_total.mean(),
    color="black", linestyle="solid", lw=1,
)
ax.loglog(
    crs_bbh.indep_variable, crs_bbh.mean(),
    color="black", linestyle="solid", lw=1, alpha=0,
)
ax.loglog(
    crs_bns.indep_variable, crs_bns.mean(),
    color="black", linestyle="solid", lw=1, alpha=0,
)
ax.loglog(
    crs_nsbh.indep_variable, crs_nsbh.mean(),
    color="black", linestyle="solid", lw=1, alpha=0,
)

# Label axes
ax.set_xlabel("Primary mass [M$_\\odot$]", fontsize=14)
ax.set_ylabel("Mass spectrum", fontsize=14)

# Limit the number of orders of magnitude for readability
limit_ax_oom(ax, n_oom, "y")

# Save to file
fig.savefig("../img/MultiSource_m1_traces.png")

# Plot individual subpopulations
fig, ax = plt.subplots(figsize=(6,5), constrained_layout=True)

# Plot traces.
ax.loglog(
    crs_total.indep_variable, crs_total.curves.T,
    color=color_total, alpha=0,
)
ax.loglog(
    crs_bbh.indep_variable, crs_bbh.curves.T,
    color=color_bbh, alpha=0.05,
)
ax.loglog(
    crs_bns.indep_variable, crs_bns.curves.T,
    color=color_bns, alpha=0.05,
)
ax.loglog(
    crs_nsbh.indep_variable, crs_nsbh.curves.T,
    color=color_nsbh, alpha=0.05,
)

# Plot credible intervals
ax.loglog(
    crs_total.indep_variable, np.transpose(crs_total.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1, alpha=0,
)
ax.loglog(
    crs_bbh.indep_variable, np.transpose(crs_bbh.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)
ax.loglog(
    crs_bns.indep_variable, np.transpose(crs_bns.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)
ax.loglog(
    crs_nsbh.indep_variable, np.transpose(crs_nsbh.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)


# Plot posterior predictive distribution
ax.loglog(
    crs_total.indep_variable, crs_total.mean(),
    color="black", linestyle="solid", lw=1, alpha=0,
)
ax.loglog(
    crs_bbh.indep_variable, crs_bbh.mean(),
    color="black", linestyle="solid", lw=1,
)
ax.loglog(
    crs_bns.indep_variable, crs_bns.mean(),
    color="black", linestyle="solid", lw=1,
)
ax.loglog(
    crs_nsbh.indep_variable, crs_nsbh.mean(),
    color="black", linestyle="solid", lw=1,
)

# Label axes
ax.set_xlabel("Primary mass [M$_\\odot$]", fontsize=14)
ax.set_ylabel("Mass spectrum", fontsize=14)

# Limit the number of orders of magnitude for readability
limit_ax_oom(ax, n_oom, "y")

# Save to file
fig.savefig("../img/MultiSource_m1_traces_by_subpop.png")


## MultiSource+GW190426 results ################################################

# Load in credible regions data
crs_fname = "../runs/MultiSource+GW190426_152155/dist_pm1_BBH.hdf5"
crs_bbh = CredibleRegions1D.load(crs_fname)
crs_fname = "../runs/MultiSource+GW190426_152155/dist_pm1_NSBH.hdf5"
crs_nsbh = CredibleRegions1D.load(crs_fname)
crs_fname = "../runs/MultiSource+GW190426_152155/dist_pm1_BNS.hdf5"
crs_bns = CredibleRegions1D.load(crs_fname)
crs_fname = "../runs/MultiSource+GW190426_152155/dist_pm1.hdf5"
crs_total = CredibleRegions1D.load(crs_fname)

# Plot traces for total population and individual subpopulations on separate
# files.  To ensure the two line up exactly, we plot everything on both figures,
# but make some elements transparent.

# Plot overall distribution
fig, ax = plt.subplots(figsize=(6,5), constrained_layout=True)

# Plot traces.
ax.loglog(
    crs_total.indep_variable, crs_total.curves.T,
    color=color_total, alpha=0.05,
)
ax.loglog(
    crs_bbh.indep_variable, crs_bbh.curves.T,
    color=color_bbh, alpha=0,
)
ax.loglog(
    crs_bns.indep_variable, crs_bns.curves.T,
    color=color_bns, alpha=0,
)
ax.loglog(
    crs_nsbh.indep_variable, crs_nsbh.curves.T,
    color=color_nsbh, alpha=0,
)

# Plot credible intervals
ax.loglog(
    crs_total.indep_variable, np.transpose(crs_total.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)
ax.loglog(
    crs_bbh.indep_variable, np.transpose(crs_bbh.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1, alpha=0,
)
ax.loglog(
    crs_bns.indep_variable, np.transpose(crs_bns.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1, alpha=0,
)
ax.loglog(
    crs_nsbh.indep_variable, np.transpose(crs_nsbh.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1, alpha=0,
)


# Plot posterior predictive distribution
ax.loglog(
    crs_total.indep_variable, crs_total.mean(),
    color="black", linestyle="solid", lw=1,
)
ax.loglog(
    crs_bbh.indep_variable, crs_bbh.mean(),
    color="black", linestyle="solid", lw=1, alpha=0,
)
ax.loglog(
    crs_bns.indep_variable, crs_bns.mean(),
    color="black", linestyle="solid", lw=1, alpha=0,
)
ax.loglog(
    crs_nsbh.indep_variable, crs_nsbh.mean(),
    color="black", linestyle="solid", lw=1, alpha=0,
)

# Label axes
ax.set_xlabel("Primary mass [M$_\\odot$]", fontsize=14)
ax.set_ylabel("Mass spectrum", fontsize=14)

# Limit the number of orders of magnitude for readability
limit_ax_oom(ax, n_oom, "y")

# Save to file
fig.savefig("../img/MultiSource+GW190426_152155_m1_traces.png")

# Plot individual subpopulations
fig, ax = plt.subplots(figsize=(6,5), constrained_layout=True)

# Plot traces.
ax.loglog(
    crs_total.indep_variable, crs_total.curves.T,
    color=color_total, alpha=0,
)
ax.loglog(
    crs_bbh.indep_variable, crs_bbh.curves.T,
    color=color_bbh, alpha=0.05,
)
ax.loglog(
    crs_bns.indep_variable, crs_bns.curves.T,
    color=color_bns, alpha=0.05,
)
ax.loglog(
    crs_nsbh.indep_variable, crs_nsbh.curves.T,
    color=color_nsbh, alpha=0.05,
)

# Plot credible intervals
ax.loglog(
    crs_total.indep_variable, np.transpose(crs_total.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1, alpha=0,
)
ax.loglog(
    crs_bbh.indep_variable, np.transpose(crs_bbh.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)
ax.loglog(
    crs_bns.indep_variable, np.transpose(crs_bns.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)
ax.loglog(
    crs_nsbh.indep_variable, np.transpose(crs_nsbh.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)


# Plot posterior predictive distribution
ax.loglog(
    crs_total.indep_variable, crs_total.mean(),
    color="black", linestyle="solid", lw=1, alpha=0,
)
ax.loglog(
    crs_bbh.indep_variable, crs_bbh.mean(),
    color="black", linestyle="solid", lw=1,
)
ax.loglog(
    crs_bns.indep_variable, crs_bns.mean(),
    color="black", linestyle="solid", lw=1,
)
ax.loglog(
    crs_nsbh.indep_variable, crs_nsbh.mean(),
    color="black", linestyle="solid", lw=1,
)

# Label axes
ax.set_xlabel("Primary mass [M$_\\odot$]", fontsize=14)
ax.set_ylabel("Mass spectrum", fontsize=14)

# Limit the number of orders of magnitude for readability
limit_ax_oom(ax, n_oom, "y")

# Save to file
fig.savefig("../img/MultiSource+GW190426_152155_m1_traces_by_subpop.png")
