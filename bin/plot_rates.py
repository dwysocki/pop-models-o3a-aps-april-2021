import numpy as np
import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from pop_models.posterior import H5CleanedPosteriorSamples


# Colors for subpopulations (pl and g)
color_bbh_pl = "#DE8F05"
color_bbh_g = "#0173B2"
color_bbh = "#029E73"
color_bns = "#D55E00"
color_nsbh = "#CC78BC"

# Linestyles for whether or not GW190426 is included
linestyle_default = "solid"
linestyle_190426 = "dashed"


## MultiSpin results ###########################################################

# Load in posteriors.
post_fname = "../runs/MultiSpin/post_cleaned.hdf5"
post_samples = H5CleanedPosteriorSamples(post_fname).get_params(...)

# Extract rate samples.
rate_bbh_pl = post_samples["rate_pl0"]
rate_bbh_g = post_samples["rate_g0"]

rate_bbh = rate_bbh_pl + rate_bbh_g

# Set up histogram bins.
rate_min = min(
    rate_bbh_pl.min(),
    rate_bbh_g.min(),
)
rate_max = rate_bbh.max()
rate_bins = np.logspace(np.log10(rate_min), np.log10(rate_max), 40)

# Make the plot
fig, ax = plt.subplots(figsize=(6,5), constrained_layout=True)

ax.hist(
    rate_bbh_pl, bins=rate_bins,
    color=color_bbh_pl,
    histtype="step",
)
ax.hist(
    rate_bbh_g, bins=rate_bins,
    color=color_bbh_g,
    histtype="step",
)
ax.hist(
    rate_bbh, bins=rate_bins,
    color=color_bbh,
    histtype="step",
)

# Plot rates in log-scale
ax.set_xscale("log")

# Hide un-necessary y-axis tick labels
ax.set_yticklabels([])

# Label axes
ax.set_xlabel("Rate [Gpc$^{-3}$ yr$^{-1}$]", fontsize=14)
ax.set_ylabel("Posterior", fontsize=14)

# Display legend
legend_handles = [
    Line2D([0], [0], color=color_bbh_pl, lw=2),
    Line2D([0], [0], color=color_bbh_g, lw=2),
    Line2D([0], [0], color=color_bbh, lw=2),
]
legend_labels = [
    "Power law",
    "Gaussian",
    "Total",
]
ax.legend(
    legend_handles, legend_labels,
    loc='upper left', bbox_to_anchor=(0.05, 1.05),
    fancybox=True, shadow=True,
)


# Save figure
fig.savefig("../img/MultiSpin_rates.png")


## MultiSource results #########################################################

# Load in posteriors
post_fname = "../runs/MultiSource/post_cleaned.hdf5"
post_samples_default = H5CleanedPosteriorSamples(post_fname).get_params(...)
post_fname = "../runs/MultiSource+GW190426_152155/post_cleaned.hdf5"
post_samples_190426 = H5CleanedPosteriorSamples(post_fname).get_params(...)

# Extract rate samples
rate_bbh_pl_default = post_samples_default["rate_pl0"]
rate_bbh_g_default = post_samples_default["rate_g0"]
rate_nsbh_default = post_samples_default["rate_g1"]
rate_bns_default = post_samples_default["rate_g2"]

rate_bbh_pl_190426 = post_samples_190426["rate_pl0"]
rate_bbh_g_190426 = post_samples_190426["rate_g0"]
rate_nsbh_190426 = post_samples_190426["rate_g1"]
rate_bns_190426 = post_samples_190426["rate_g2"]

rate_bbh_default = rate_bbh_pl_default + rate_bbh_g_default
rate_bbh_190426 = rate_bbh_pl_190426 + rate_bbh_g_190426

# Set up histogram bins
rate_min = min(
    rate_bbh_pl_default.min(), rate_bbh_g_default.min(),
    rate_nsbh_default.min(),
    rate_bns_default.min(),
    rate_bbh_pl_190426.min(), rate_bbh_g_190426.min(),
    rate_nsbh_190426.min(),
    rate_bns_190426.min(),
)
rate_max = max(
    rate_bbh_default.max(),
    rate_nsbh_default.max(),
    rate_bns_default.max(),
    rate_bbh_190426.max(),
    rate_nsbh_190426.max(),
    rate_bns_190426.max(),
)
rate_bins = np.logspace(np.log10(rate_min), np.log10(rate_max), 40)

# Make the plot
fig, ax = plt.subplots(figsize=(6,5), constrained_layout=True)

ax.hist(
    rate_bbh_default, bins=rate_bins,
    color=color_bbh, linestyle=linestyle_default,
    histtype="step",
)
ax.hist(
    rate_bns_default, bins=rate_bins,
    color=color_bns, linestyle=linestyle_default,
    histtype="step",
)
ax.hist(
    rate_nsbh_default, bins=rate_bins,
    color=color_nsbh, linestyle=linestyle_default,
    histtype="step",
)

# Plot rates in log-scale
ax.set_xscale("log")

# Hide un-necessary y-axis tick labels
ax.set_yticklabels([])

# Label axes
ax.set_xlabel("Rate [Gpc$^{-3}$ yr$^{-1}$]", fontsize=14)
ax.set_ylabel("Posterior", fontsize=14)

# Display legends
legend_handles = [
    Line2D([0], [0], color=color_bbh, lw=2),
    Line2D([0], [0], color=color_bns, lw=2),
    Line2D([0], [0], color=color_nsbh, lw=2),
]
legend_labels = [
    "BBH",
    "BNS",
    "NSBH",
]
ax.legend(
    legend_handles, legend_labels,
    loc='upper left', bbox_to_anchor=(0.05, 1.05),
    fancybox=True, shadow=True,
)

# Save figure
fig.savefig("../img/MultiSource_rates.png")
