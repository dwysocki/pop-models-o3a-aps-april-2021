import numpy as np
import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from pop_models.posterior import H5CleanedPosteriorSamples

# Colors and linestyles for subpopulations (pl and g) and binary components
# (1 and 2).
color_bbh_pl = "#DE8F05"
color_bbh_g = "#0173B2"
color_bns = "#D55E00"
color_nsbh = "#CC78BC"
linestyle_1 = "solid"
linestyle_2 = "dashed"
# Color and linestyle for prior overlay.
color_prior = "#949494"
linestyle_prior = "dotted"

## MultiSpin Results ###########################################################

# Load in posteriors.
post_fname = "../runs/MultiSpin/post_cleaned.hdf5"
post_samples = H5CleanedPosteriorSamples(post_fname).get_params(...)

# Set up plot
fig, axes = plt.subplots(1, 3, figsize=(8,5), constrained_layout=True)
ax_mu_chi, ax_var_chi, ax_sigma_cos = axes

# Plot histograms
bins_mu_chi = np.linspace(0.0, 1.0, 20)
bins_var_chi = np.linspace(0.0, 0.1, 20)
bins_sigma_cos = np.linspace(0.0, 4.0, 20)
# Plot spin magnitude mean hyperparameter posteriors
ax_mu_chi.hist(
    post_samples["E_chi1_pl0"], bins=bins_mu_chi,
    color=color_bbh_pl, linestyle=linestyle_1,
    histtype="step", density=True,
)
ax_mu_chi.hist(
    post_samples["E_chi2_pl0"], bins=bins_mu_chi,
    color=color_bbh_pl, linestyle=linestyle_2,
    histtype="step", density=True,
)
ax_mu_chi.hist(
    post_samples["E_chi1_g0"], bins=bins_mu_chi,
    color=color_bbh_g, linestyle=linestyle_1,
    histtype="step", density=True,
)
ax_mu_chi.hist(
    post_samples["E_chi2_g0"], bins=bins_mu_chi,
    color=color_bbh_g, linestyle=linestyle_2,
    histtype="step", density=True,
)
# Plot spin magnitude variance hyperparameter posteriors
ax_var_chi.hist(
    post_samples["Var_chi1_pl0"], bins=bins_var_chi,
    color=color_bbh_pl, linestyle=linestyle_1,
    histtype="step", density=True,
)
ax_var_chi.hist(
    post_samples["Var_chi2_pl0"], bins=bins_var_chi,
    color=color_bbh_pl, linestyle=linestyle_2,
    histtype="step", density=True,
)
ax_var_chi.hist(
    post_samples["Var_chi1_g0"], bins=bins_var_chi,
    color=color_bbh_g, linestyle=linestyle_1,
    histtype="step", density=True,
)
ax_var_chi.hist(
    post_samples["Var_chi2_g0"], bins=bins_var_chi,
    color=color_bbh_g, linestyle=linestyle_2,
    histtype="step", density=True,
)
# Plot spin tilt hyperparameter posteriors
ax_sigma_cos.hist(
    post_samples["sigma_cos1_pl0"], bins=bins_sigma_cos,
    color=color_bbh_pl, linestyle=linestyle_1,
    histtype="step", density=True,
)
ax_sigma_cos.hist(
    post_samples["sigma_cos2_pl0"], bins=bins_sigma_cos,
    color=color_bbh_pl, linestyle=linestyle_2,
    histtype="step", density=True,
)
ax_sigma_cos.hist(
    post_samples["sigma_cos1_g0"], bins=bins_sigma_cos,
    color=color_bbh_g, linestyle=linestyle_1,
    histtype="step", density=True,
)
ax_sigma_cos.hist(
    post_samples["sigma_cos2_g0"], bins=bins_sigma_cos,
    color=color_bbh_g, linestyle=linestyle_2,
    histtype="step", density=True,
)

# Remove unhelpful y-axis units
for ax in axes:
    ax.set_yticklabels([])

# Add axis labels
ax_mu_chi.set_ylabel("Posterior", fontsize=16)
ax_mu_chi.set_xlabel("Mean $\\chi$", fontsize=14)
ax_var_chi.set_xlabel("Variance $\\chi$", fontsize=14)
ax_sigma_cos.set_xlabel("Cos(Tilt) width $\\sigma$", fontsize=14)

# Add legend for subpopulations
legend_handles = [
    Line2D([0], [0], color=color_bbh_pl, lw=2),
    Line2D([0], [0], color=color_bbh_g, lw=2),
]
legend_labels = [
    "Power law",
    "Gaussian",
]
ax_mu_chi.legend(
    legend_handles, legend_labels,
    loc='upper center', bbox_to_anchor=(0.5, 1.05),
    fancybox=True, shadow=True,
)
# Add legend for binary component linestyles
legend_handles = [
    Line2D([0], [0], color="black", linestyle=linestyle_1, lw=2),
    Line2D([0], [0], color="black", linestyle=linestyle_2, lw=2),
]
legend_labels = [
    "Component 1",
    "Component 2",
]
ax_var_chi.legend(
    legend_handles, legend_labels,
    loc='upper center', bbox_to_anchor=(0.5, 1.05),
    fancybox=True, shadow=True,
)
# # Add legend for prior overlay
# legend_handles = [
#     Line2D([0], [0], color=color_prior, linestyle=linestyle_prior, lw=2),
# ]
# legend_labels = [
#     "Prior",
# ]
# ax_sigma_cos.legend(
#     legend_handles, legend_labels,
#     loc='upper center', bbox_to_anchor=(0.5, 1.05),
#     fancybox=True, shadow=True,
# )

# Save plot to file
fig.savefig("../img/MultiSpin_spin_hyperparameters.png")

## MultiSource Results #########################################################

# Load in posteriors.
post_fname = "../runs/MultiSource/post_cleaned.hdf5"
post_samples = H5CleanedPosteriorSamples(post_fname).get_params(...)

# Set up BH plot
fig, axes = plt.subplots(1, 3, figsize=(8,5), constrained_layout=True)
ax_mu_chi, ax_var_chi, ax_sigma_cos = axes

# Plot histograms
bins_mu_chi = np.linspace(0.0, 1.0, 20)
bins_var_chi = np.linspace(0.0, 0.1, 20)
bins_sigma_cos = np.linspace(0.0, 4.0, 20)
# Plot spin magnitude mean hyperparameter posteriors
ax_mu_chi.hist(
    post_samples["E_chi1_pl0"], bins=bins_mu_chi,
    color=color_bbh_pl, linestyle=linestyle_1,
    histtype="step", density=True,
)
ax_mu_chi.hist(
    post_samples["E_chi2_pl0"], bins=bins_mu_chi,
    color=color_bbh_pl, linestyle=linestyle_2,
    histtype="step", density=True,
)
ax_mu_chi.hist(
    post_samples["E_chi1_g0"], bins=bins_mu_chi,
    color=color_bbh_g, linestyle=linestyle_1,
    histtype="step", density=True,
)
ax_mu_chi.hist(
    post_samples["E_chi2_g0"], bins=bins_mu_chi,
    color=color_bbh_g, linestyle=linestyle_2,
    histtype="step", density=True,
)
ax_mu_chi.hist(
    post_samples["E_chi1_g1"], bins=bins_mu_chi,
    color=color_nsbh, linestyle=linestyle_1,
    histtype="step", density=True,
)

# Plot spin magnitude variance hyperparameter posteriors
ax_var_chi.hist(
    post_samples["Var_chi1_pl0"], bins=bins_var_chi,
    color=color_bbh_pl, linestyle=linestyle_1,
    histtype="step", density=True,
)
ax_var_chi.hist(
    post_samples["Var_chi2_pl0"], bins=bins_var_chi,
    color=color_bbh_pl, linestyle=linestyle_2,
    histtype="step", density=True,
)
ax_var_chi.hist(
    post_samples["Var_chi1_g0"], bins=bins_var_chi,
    color=color_bbh_g, linestyle=linestyle_1,
    histtype="step", density=True,
)
ax_var_chi.hist(
    post_samples["Var_chi2_g0"], bins=bins_var_chi,
    color=color_bbh_g, linestyle=linestyle_2,
    histtype="step", density=True,
)
ax_var_chi.hist(
    post_samples["Var_chi1_g1"], bins=bins_var_chi,
    color=color_nsbh, linestyle=linestyle_1,
    histtype="step", density=True,
)
# Plot spin tilt hyperparameter posteriors
ax_sigma_cos.hist(
    post_samples["sigma_cos1_pl0"], bins=bins_sigma_cos,
    color=color_bbh_pl, linestyle=linestyle_1,
    histtype="step", density=True,
)
ax_sigma_cos.hist(
    post_samples["sigma_cos2_pl0"], bins=bins_sigma_cos,
    color=color_bbh_pl, linestyle=linestyle_2,
    histtype="step", density=True,
)
ax_sigma_cos.hist(
    post_samples["sigma_cos1_g0"], bins=bins_sigma_cos,
    color=color_bbh_g, linestyle=linestyle_1,
    histtype="step", density=True,
)
ax_sigma_cos.hist(
    post_samples["sigma_cos2_g0"], bins=bins_sigma_cos,
    color=color_bbh_g, linestyle=linestyle_2,
    histtype="step", density=True,
)
ax_sigma_cos.hist(
    post_samples["sigma_cos1_g1"], bins=bins_sigma_cos,
    color=color_nsbh, linestyle=linestyle_1,
    histtype="step", density=True,
)

# Remove unhelpful y-axis units
for ax in axes:
    ax.set_yticklabels([])

# Add axis labels
ax_mu_chi.set_ylabel("Posterior", fontsize=16)
ax_mu_chi.set_xlabel("Mean $\\chi$", fontsize=14)
ax_var_chi.set_xlabel("Variance $\\chi$", fontsize=14)
ax_sigma_cos.set_xlabel("Cos(Tilt) width $\\sigma$", fontsize=14)

# Add legend for subpopulations
legend_handles = [
    Line2D([0], [0], color=color_bbh_pl, lw=2),
    Line2D([0], [0], color=color_bbh_g, lw=2),
    Line2D([0], [0], color=color_nsbh, lw=2),
]
legend_labels = [
    "BBH power law",
    "BBH Gaussian",
    "NSBH",
]
ax_mu_chi.legend(
    legend_handles, legend_labels,
    loc='upper center', bbox_to_anchor=(0.5, 1.05),
    fancybox=True, shadow=True,
)
# Add legend for binary component linestyles
legend_handles = [
    Line2D([0], [0], color="black", linestyle=linestyle_1, lw=2),
    Line2D([0], [0], color="black", linestyle=linestyle_2, lw=2),
]
legend_labels = [
    "Component 1",
    "Component 2",
]
ax_var_chi.legend(
    legend_handles, legend_labels,
    loc='upper center', bbox_to_anchor=(0.5, 1.05),
    fancybox=True, shadow=True,
)
# # Add legend for prior overlay
# legend_handles = [
#     Line2D([0], [0], color=color_prior, linestyle=linestyle_prior, lw=2),
# ]
# legend_labels = [
#     "Prior",
# ]
# ax_sigma_cos.legend(
#     legend_handles, legend_labels,
#     loc='upper center', bbox_to_anchor=(0.5, 1.05),
#     fancybox=True, shadow=True,
# )

# Save plot to file
fig.savefig("../img/MultiSource_BH_spin_hyperparameters.png")


# Set up NS plot
fig, axes = plt.subplots(1, 2, figsize=(5,5), constrained_layout=True)
ax_mu_chi, ax_var_chi = axes

# Plot histograms
bins_mu_chi = np.linspace(0.0, 0.05, 20)
bins_var_chi = np.linspace(0.0, 0.00025, 20)
# Plot spin magnitude mean hyperparameter posteriors
ax_mu_chi.hist(
    post_samples["E_chi1_g2"], bins=bins_mu_chi,
    color=color_bns,
    histtype="step", density=True,
)
# Plot spin magnitude variance hyperparameter posteriors
ax_var_chi.hist(
    post_samples["Var_chi1_g2"], bins=bins_var_chi,
    color=color_bns,
    histtype="step", density=True,
)

# Remove unhelpful y-axis units
for ax in axes:
    ax.set_yticklabels([])

# Add axis labels
ax_mu_chi.set_ylabel("Posterior", fontsize=16)
ax_mu_chi.set_xlabel("Mean $\\chi$", fontsize=14)
ax_var_chi.set_xlabel("Variance $\\chi$", fontsize=14)

# Add legend for subpopulations
legend_handles = [
    Line2D([0], [0], color=color_bns, lw=2),
]
legend_labels = [
    "NS",
]
ax_mu_chi.legend(
    legend_handles, legend_labels,
    loc='upper center', bbox_to_anchor=(0.5, 1.05),
    fancybox=True, shadow=True,
)
# # Add legend for prior overlay
# legend_handles = [
#     Line2D([0], [0], color=color_prior, linestyle=linestyle_prior, lw=2),
# ]
# legend_labels = [
#     "Prior",
# ]
# ax_var_chi.legend(
#     legend_handles, legend_labels,
#     loc='upper center', bbox_to_anchor=(0.5, 1.05),
#     fancybox=True, shadow=True,
# )

# Save plot to file
fig.savefig("../img/MultiSource_NS_spin_hyperparameters.png")
