import numpy as np
import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from pop_models.credible_regions import CredibleRegions1D
from pop_models.utils.plotting import limit_ax_oom

# Limit number of OoM in distribution plots so log-log scale doesn't kill us.
n_oom = 5

# Colors for subpopulations (pl and g)
color_bbh_pl = "#DE8F05"
color_bbh_g = "#0173B2"
color_bbh = "#029E73"
color_bns = "#D55E00"
color_nsbh = "#CC78BC"
color_total = "#56B4E9"

# Linestyles for whether or not GW190426 is included
linestyle_default = "solid"
linestyle_190426 = "dashed"

# x-axis formatters
integer_formatter = mpl.ticker.StrMethodFormatter("{x:.0f}")
major_xaxis_formatter = integer_formatter
minor_xaxis_formatter = mpl.ticker.NullFormatter()

# x-axis minor tick locations and labels
xaxis_ticks = [1, 2, 3, 4, 5, 10, 20, 30, 40, 50, 100]

mass_range_min, mass_range_max = 1.0, 100.0

def export_legend(legend, filename="legend.png", expand=[-5,-5,5,5]):
    """
    Save only a legend to a file.

    Taken from https://stackoverflow.com/a/47749903
    """
    fig  = legend.figure
    fig.canvas.draw()
    bbox  = legend.get_window_extent()
    bbox = bbox.from_extents(*(bbox.extents + np.array(expand)))
    bbox = bbox.transformed(fig.dpi_scale_trans.inverted())
    fig.savefig(filename, dpi="figure", bbox_inches=bbox, transparent=True)


## MultiSpin results ###########################################################

# Load in credible regions data
crs_fname = "../runs/MultiSpin/dist_Rpm1_pl0.hdf5"
crs_bbh_pl = CredibleRegions1D.load(crs_fname)
crs_fname = "../runs/MultiSpin/dist_Rpm1_g0.hdf5"
crs_bbh_g = CredibleRegions1D.load(crs_fname)
crs_fname = "../runs/MultiSpin/dist_Rpm1.hdf5"
crs_bbh = CredibleRegions1D.load(crs_fname)

# Plot traces for total population and individual subpopulations on separate
# files.  To ensure the two line up exactly, we plot everything on both figures,
# but make some elements transparent.

# Plot overall distribution
fig, ax = plt.subplots(figsize=(6,5), constrained_layout=True)

# Plot traces.
ax.loglog(
    crs_bbh.indep_variable, crs_bbh.curves.T,
    color=color_bbh, alpha=0.05,
)
ax.loglog(
    crs_bbh_pl.indep_variable, crs_bbh_pl.curves.T,
    color=color_bbh_pl, alpha=0,
)
ax.loglog(
    crs_bbh_g.indep_variable, crs_bbh_g.curves.T,
    color=color_bbh_g, alpha=0,
)

# Plot credible intervals
ax.loglog(
    crs_bbh.indep_variable, np.transpose(crs_bbh.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)
ax.loglog(
    crs_bbh_pl.indep_variable, np.transpose(crs_bbh_pl.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1, alpha=0,
)
ax.loglog(
    crs_bbh_g.indep_variable, np.transpose(crs_bbh_g.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1, alpha=0,
)

# Plot posterior predictive distribution
ax.loglog(
    crs_bbh.indep_variable, crs_bbh.mean(),
    color="black", linestyle="solid", lw=1,
)
ax.loglog(
    crs_bbh_pl.indep_variable, crs_bbh_pl.mean(),
    color="black", linestyle="solid", lw=1, alpha=0,
)
ax.loglog(
    crs_bbh_g.indep_variable, crs_bbh_g.mean(),
    color="black", linestyle="solid", lw=1, alpha=0,
)

# Label axes
ax.set_xlabel("Primary mass [M$_\\odot$]", fontsize=14)
ax.set_ylabel("Differential Rate [Gpc$^{-3}$ yr$^{-1}$ $M_\\odot^{-1}$]", fontsize=14)

# Label the plot
ax.legend([], [], loc="upper right", title="Overall", frameon=False)

# Limit the number of orders of magnitude for readability
limit_ax_oom(ax, n_oom, "y")

# Truncate the x-axis
ax.set_xlim(mass_range_min, mass_range_max)

# Set custom tick locations
ax.set_xticks(xaxis_ticks)

# Don't use scientific notation for masses despite the log scale
ax.xaxis.set_major_formatter(major_xaxis_formatter)
ax.xaxis.set_minor_formatter(minor_xaxis_formatter)

# Save to file
fig.savefig("../img/MultiSpin_m1_traces.png")


# Plot individual subpopulations
fig, ax = plt.subplots(figsize=(6,5), constrained_layout=True)

# Plot traces.
ax.loglog(
    crs_bbh.indep_variable, crs_bbh.curves.T,
    color=color_bbh, alpha=0,
)
ax.loglog(
    crs_bbh_pl.indep_variable, crs_bbh_pl.curves.T,
    color=color_bbh_pl, alpha=0.05,
)
ax.loglog(
    crs_bbh_g.indep_variable, crs_bbh_g.curves.T,
    color=color_bbh_g, alpha=0.05,
)

# Plot credible intervals
ax.loglog(
    crs_bbh.indep_variable, np.transpose(crs_bbh.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1, alpha=0,
)
ax.loglog(
    crs_bbh_pl.indep_variable, np.transpose(crs_bbh_pl.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)
ax.loglog(
    crs_bbh_g.indep_variable, np.transpose(crs_bbh_g.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)

# Plot posterior predictive distribution
ax.loglog(
    crs_bbh.indep_variable, crs_bbh.mean(),
    color="black", linestyle="solid", lw=1, alpha=0,
)
ax.loglog(
    crs_bbh_pl.indep_variable, crs_bbh_pl.mean(),
    color="black", linestyle="solid", lw=1,
)
ax.loglog(
    crs_bbh_g.indep_variable, crs_bbh_g.mean(),
    color="black", linestyle="solid", lw=1,
)

# Label axes
ax.set_xlabel("Primary mass [M$_\\odot$]", fontsize=14)
ax.set_ylabel("Differential Rate [Gpc$^{-3}$ yr$^{-1}$ $M_\\odot^{-1}$]", fontsize=14)

# Label the plot
ax.legend([], [], loc="upper right", title="By Subpopulation", frameon=False)

# Limit the number of orders of magnitude for readability
limit_ax_oom(ax, n_oom, "y")

# Truncate the x-axis
ax.set_xlim(mass_range_min, mass_range_max)

# Set custom tick locations
ax.set_xticks(xaxis_ticks)

# Don't use scientific notation for masses despite the log scale
ax.xaxis.set_major_formatter(major_xaxis_formatter)
ax.xaxis.set_minor_formatter(minor_xaxis_formatter)

# Save to file
fig.savefig("../img/MultiSpin_m1_traces_by_subpop.png")



## MultiSource results #########################################################

# Load in credible regions data
crs_fname = "../runs/MultiSource/dist_Rpm1_BBH.hdf5"
crs_bbh = CredibleRegions1D.load(crs_fname)
crs_fname = "../runs/MultiSource/dist_Rpm1_NSBH.hdf5"
crs_nsbh = CredibleRegions1D.load(crs_fname)
crs_fname = "../runs/MultiSource/dist_Rpm1_BNS.hdf5"
crs_bns = CredibleRegions1D.load(crs_fname)
crs_fname = "../runs/MultiSource/dist_Rpm1.hdf5"
crs_total = CredibleRegions1D.load(crs_fname)

# Plot traces for total population and individual subpopulations on separate
# files.  To ensure the two line up exactly, we plot everything on both figures,
# but make some elements transparent.

# Plot overall distribution
fig, ax = plt.subplots(figsize=(6,5), constrained_layout=True)

# Plot traces.
ax.loglog(
    crs_total.indep_variable, crs_total.curves.T,
    color=color_total, alpha=0.05,
)
ax.loglog(
    crs_bbh.indep_variable, crs_bbh.curves.T,
    color=color_bbh, alpha=0,
)
ax.loglog(
    crs_bns.indep_variable, crs_bns.curves.T,
    color=color_bns, alpha=0,
)
ax.loglog(
    crs_nsbh.indep_variable, crs_nsbh.curves.T,
    color=color_nsbh, alpha=0,
)

# Plot credible intervals
ax.loglog(
    crs_total.indep_variable, np.transpose(crs_total.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)
ax.loglog(
    crs_bbh.indep_variable, np.transpose(crs_bbh.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1, alpha=0,
)
ax.loglog(
    crs_bns.indep_variable, np.transpose(crs_bns.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1, alpha=0,
)
ax.loglog(
    crs_nsbh.indep_variable, np.transpose(crs_nsbh.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1, alpha=0,
)


# Plot posterior predictive distribution
ax.loglog(
    crs_total.indep_variable, crs_total.mean(),
    color="black", linestyle="solid", lw=1,
)
ax.loglog(
    crs_bbh.indep_variable, crs_bbh.mean(),
    color="black", linestyle="solid", lw=1, alpha=0,
)
ax.loglog(
    crs_bns.indep_variable, crs_bns.mean(),
    color="black", linestyle="solid", lw=1, alpha=0,
)
ax.loglog(
    crs_nsbh.indep_variable, crs_nsbh.mean(),
    color="black", linestyle="solid", lw=1, alpha=0,
)

# Label axes
ax.set_xlabel("Primary mass [M$_\\odot$]", fontsize=14)
ax.set_ylabel("Differential Rate [Gpc$^{-3}$ yr$^{-1}$ $M_\\odot^{-1}$]", fontsize=14)

# Label the plot
ax.legend([], [], loc="upper right", title="Overall", frameon=False)

# Limit the number of orders of magnitude for readability
limit_ax_oom(ax, n_oom, "y")

# Truncate the x-axis
ax.set_xlim(mass_range_min, mass_range_max)

# Set custom tick locations
ax.set_xticks(xaxis_ticks)

# Don't use scientific notation for masses despite the log scale
ax.xaxis.set_major_formatter(major_xaxis_formatter)
ax.xaxis.set_minor_formatter(minor_xaxis_formatter)

# Save to file
fig.savefig("../img/MultiSource_m1_traces.png")

# Plot individual subpopulations
fig, ax = plt.subplots(figsize=(6,5), constrained_layout=True)

# Plot traces.
ax.loglog(
    crs_total.indep_variable, crs_total.curves.T,
    color=color_total, alpha=0,
)
ax.loglog(
    crs_bbh.indep_variable, crs_bbh.curves.T,
    color=color_bbh, alpha=0.05,
)
ax.loglog(
    crs_bns.indep_variable, crs_bns.curves.T,
    color=color_bns, alpha=0.05,
)
ax.loglog(
    crs_nsbh.indep_variable, crs_nsbh.curves.T,
    color=color_nsbh, alpha=0.05,
)

# Plot credible intervals
ax.loglog(
    crs_total.indep_variable, np.transpose(crs_total.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1, alpha=0,
)
ax.loglog(
    crs_bbh.indep_variable, np.transpose(crs_bbh.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)
ax.loglog(
    crs_bns.indep_variable, np.transpose(crs_bns.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)
ax.loglog(
    crs_nsbh.indep_variable, np.transpose(crs_nsbh.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)


# Plot posterior predictive distribution
ax.loglog(
    crs_total.indep_variable, crs_total.mean(),
    color="black", linestyle="solid", lw=1, alpha=0,
)
ax.loglog(
    crs_bbh.indep_variable, crs_bbh.mean(),
    color="black", linestyle="solid", lw=1,
)
ax.loglog(
    crs_bns.indep_variable, crs_bns.mean(),
    color="black", linestyle="solid", lw=1,
)
ax.loglog(
    crs_nsbh.indep_variable, crs_nsbh.mean(),
    color="black", linestyle="solid", lw=1,
)

# Label axes
ax.set_xlabel("Primary mass [M$_\\odot$]", fontsize=14)
ax.set_ylabel("Differential Rate [Gpc$^{-3}$ yr$^{-1}$ $M_\\odot^{-1}$]", fontsize=14)

# Label the plot
ax.legend([], [], loc="upper right", title="By Subpopulation", frameon=False)

# Limit the number of orders of magnitude for readability
limit_ax_oom(ax, n_oom, "y")

# Truncate the x-axis
ax.set_xlim(mass_range_min, mass_range_max)

# Set custom tick locations
ax.set_xticks(xaxis_ticks)

# Don't use scientific notation for masses despite the log scale
ax.xaxis.set_major_formatter(major_xaxis_formatter)
ax.xaxis.set_minor_formatter(minor_xaxis_formatter)

# Save to file
fig.savefig("../img/MultiSource_m1_traces_by_subpop.png")


## MultiSource+GW190426 results ################################################

# Load in credible regions data
crs_fname = "../runs/MultiSource+GW190426_152155/dist_Rpm1_BBH.hdf5"
crs_bbh = CredibleRegions1D.load(crs_fname)
crs_fname = "../runs/MultiSource+GW190426_152155/dist_Rpm1_NSBH.hdf5"
crs_nsbh = CredibleRegions1D.load(crs_fname)
crs_fname = "../runs/MultiSource+GW190426_152155/dist_Rpm1_BNS.hdf5"
crs_bns = CredibleRegions1D.load(crs_fname)
crs_fname = "../runs/MultiSource+GW190426_152155/dist_Rpm1.hdf5"
crs_total = CredibleRegions1D.load(crs_fname)

# Plot traces for total population and individual subpopulations on separate
# files.  To ensure the two line up exactly, we plot everything on both figures,
# but make some elements transparent.

# Plot overall distribution
fig, ax = plt.subplots(figsize=(6,5), constrained_layout=True)

# Plot traces.
ax.loglog(
    crs_total.indep_variable, crs_total.curves.T,
    color=color_total, alpha=0.05,
)
ax.loglog(
    crs_bbh.indep_variable, crs_bbh.curves.T,
    color=color_bbh, alpha=0,
)
ax.loglog(
    crs_bns.indep_variable, crs_bns.curves.T,
    color=color_bns, alpha=0,
)
ax.loglog(
    crs_nsbh.indep_variable, crs_nsbh.curves.T,
    color=color_nsbh, alpha=0,
)

# Plot credible intervals
ax.loglog(
    crs_total.indep_variable, np.transpose(crs_total.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)
ax.loglog(
    crs_bbh.indep_variable, np.transpose(crs_bbh.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1, alpha=0,
)
ax.loglog(
    crs_bns.indep_variable, np.transpose(crs_bns.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1, alpha=0,
)
ax.loglog(
    crs_nsbh.indep_variable, np.transpose(crs_nsbh.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1, alpha=0,
)


# Plot posterior predictive distribution
ax.loglog(
    crs_total.indep_variable, crs_total.mean(),
    color="black", linestyle="solid", lw=1,
)
ax.loglog(
    crs_bbh.indep_variable, crs_bbh.mean(),
    color="black", linestyle="solid", lw=1, alpha=0,
)
ax.loglog(
    crs_bns.indep_variable, crs_bns.mean(),
    color="black", linestyle="solid", lw=1, alpha=0,
)
ax.loglog(
    crs_nsbh.indep_variable, crs_nsbh.mean(),
    color="black", linestyle="solid", lw=1, alpha=0,
)

# Label axes
ax.set_xlabel("Primary mass [M$_\\odot$]", fontsize=14)
ax.set_ylabel("Differential Rate [Gpc$^{-3}$ yr$^{-1}$ $M_\\odot^{-1}$]", fontsize=14)

# Label the plot
ax.legend([], [], loc="upper right", title="Overall", frameon=False)

# Limit the number of orders of magnitude for readability
limit_ax_oom(ax, n_oom, "y")

# Truncate the x-axis
ax.set_xlim(mass_range_min, mass_range_max)

# Set custom tick locations
ax.set_xticks(xaxis_ticks)

# Don't use scientific notation for masses despite the log scale
ax.xaxis.set_major_formatter(major_xaxis_formatter)
ax.xaxis.set_minor_formatter(minor_xaxis_formatter)

# Save to file
fig.savefig("../img/MultiSource+GW190426_152155_m1_traces.png")

# Plot individual subpopulations
fig, ax = plt.subplots(figsize=(6,5), constrained_layout=True)

# Plot traces.
ax.loglog(
    crs_total.indep_variable, crs_total.curves.T,
    color=color_total, alpha=0,
)
ax.loglog(
    crs_bbh.indep_variable, crs_bbh.curves.T,
    color=color_bbh, alpha=0.05,
)
ax.loglog(
    crs_bns.indep_variable, crs_bns.curves.T,
    color=color_bns, alpha=0.05,
)
ax.loglog(
    crs_nsbh.indep_variable, crs_nsbh.curves.T,
    color=color_nsbh, alpha=0.05,
)

# Plot credible intervals
ax.loglog(
    crs_total.indep_variable, np.transpose(crs_total.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1, alpha=0,
)
ax.loglog(
    crs_bbh.indep_variable, np.transpose(crs_bbh.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)
ax.loglog(
    crs_bns.indep_variable, np.transpose(crs_bns.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)
ax.loglog(
    crs_nsbh.indep_variable, np.transpose(crs_nsbh.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)


# Plot posterior predictive distribution
ax.loglog(
    crs_total.indep_variable, crs_total.mean(),
    color="black", linestyle="solid", lw=1, alpha=0,
)
ax.loglog(
    crs_bbh.indep_variable, crs_bbh.mean(),
    color="black", linestyle="solid", lw=1,
)
ax.loglog(
    crs_bns.indep_variable, crs_bns.mean(),
    color="black", linestyle="solid", lw=1,
)
ax.loglog(
    crs_nsbh.indep_variable, crs_nsbh.mean(),
    color="black", linestyle="solid", lw=1,
)

# Label axes
ax.set_xlabel("Primary mass [M$_\\odot$]", fontsize=14)
ax.set_ylabel("Differential Rate [Gpc$^{-3}$ yr$^{-1}$ $M_\\odot^{-1}$]", fontsize=14)

# Label the plot
ax.legend([], [], loc="upper right", title="By Subpopulation", frameon=False)

# Limit the number of orders of magnitude for readability
limit_ax_oom(ax, n_oom, "y")

# Truncate the x-axis
ax.set_xlim(mass_range_min, mass_range_max)

# Set custom tick locations
ax.set_xticks(xaxis_ticks)

# Don't use scientific notation for masses despite the log scale
ax.xaxis.set_major_formatter(major_xaxis_formatter)
ax.xaxis.set_minor_formatter(minor_xaxis_formatter)

# Save to file
fig.savefig("../img/MultiSource+GW190426_152155_m1_traces_by_subpop.png")


## EM-informed MultiSource results #############################################

# Load in credible regions data
crs_fname = "../runs/MultiSource_EM-informed/dist_Rpm1_BBH.hdf5"
crs_bbh = CredibleRegions1D.load(crs_fname)
crs_fname = "../runs/MultiSource_EM-informed/dist_Rpm1_NSBH.hdf5"
crs_nsbh = CredibleRegions1D.load(crs_fname)
crs_fname = "../runs/MultiSource_EM-informed/dist_Rpm1_BNS.hdf5"
crs_bns = CredibleRegions1D.load(crs_fname)
crs_fname = "../runs/MultiSource_EM-informed/dist_Rpm1.hdf5"
crs_total = CredibleRegions1D.load(crs_fname)

# Plot traces for total population and individual subpopulations on separate
# files.  To ensure the two line up exactly, we plot everything on both figures,
# but make some elements transparent.

# Plot overall distribution
fig, ax = plt.subplots(figsize=(6,5), constrained_layout=True)

# Plot traces.
ax.loglog(
    crs_total.indep_variable, crs_total.curves.T,
    color=color_total, alpha=0.05,
)
ax.loglog(
    crs_bbh.indep_variable, crs_bbh.curves.T,
    color=color_bbh, alpha=0,
)
ax.loglog(
    crs_bns.indep_variable, crs_bns.curves.T,
    color=color_bns, alpha=0,
)
ax.loglog(
    crs_nsbh.indep_variable, crs_nsbh.curves.T,
    color=color_nsbh, alpha=0,
)

# Plot credible intervals
ax.loglog(
    crs_total.indep_variable, np.transpose(crs_total.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)
ax.loglog(
    crs_bbh.indep_variable, np.transpose(crs_bbh.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1, alpha=0,
)
ax.loglog(
    crs_bns.indep_variable, np.transpose(crs_bns.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1, alpha=0,
)
ax.loglog(
    crs_nsbh.indep_variable, np.transpose(crs_nsbh.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1, alpha=0,
)


# Plot posterior predictive distribution
ax.loglog(
    crs_total.indep_variable, crs_total.mean(),
    color="black", linestyle="solid", lw=1,
)
ax.loglog(
    crs_bbh.indep_variable, crs_bbh.mean(),
    color="black", linestyle="solid", lw=1, alpha=0,
)
ax.loglog(
    crs_bns.indep_variable, crs_bns.mean(),
    color="black", linestyle="solid", lw=1, alpha=0,
)
ax.loglog(
    crs_nsbh.indep_variable, crs_nsbh.mean(),
    color="black", linestyle="solid", lw=1, alpha=0,
)

# Label axes
ax.set_xlabel("Primary mass [M$_\\odot$]", fontsize=14)
ax.set_ylabel("Differential Rate [Gpc$^{-3}$ yr$^{-1}$ $M_\\odot^{-1}$]", fontsize=14)

# Label the plot
ax.legend([], [], loc="upper right", title="Overall", frameon=False)

# Limit the number of orders of magnitude for readability
limit_ax_oom(ax, n_oom, "y")

# Truncate the x-axis
ax.set_xlim(mass_range_min, mass_range_max)

# Set custom tick locations
ax.set_xticks(xaxis_ticks)

# Don't use scientific notation for masses despite the log scale
ax.xaxis.set_major_formatter(major_xaxis_formatter)
ax.xaxis.set_minor_formatter(minor_xaxis_formatter)

# Save to file
fig.savefig("../img/MultiSource_EM-informed_m1_traces.png")

# Plot individual subpopulations
fig, ax = plt.subplots(figsize=(6,5), constrained_layout=True)

# Plot traces.
ax.loglog(
    crs_total.indep_variable, crs_total.curves.T,
    color=color_total, alpha=0,
)
ax.loglog(
    crs_bbh.indep_variable, crs_bbh.curves.T,
    color=color_bbh, alpha=0.05,
)
ax.loglog(
    crs_bns.indep_variable, crs_bns.curves.T,
    color=color_bns, alpha=0.05,
)
ax.loglog(
    crs_nsbh.indep_variable, crs_nsbh.curves.T,
    color=color_nsbh, alpha=0.05,
)

# Plot credible intervals
ax.loglog(
    crs_total.indep_variable, np.transpose(crs_total.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1, alpha=0,
)
ax.loglog(
    crs_bbh.indep_variable, np.transpose(crs_bbh.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)
ax.loglog(
    crs_bns.indep_variable, np.transpose(crs_bns.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)
ax.loglog(
    crs_nsbh.indep_variable, np.transpose(crs_nsbh.equal_prob_bounds(0.9)),
    color="black", linestyle="dashed", lw=1,
)


# Plot posterior predictive distribution
ax.loglog(
    crs_total.indep_variable, crs_total.mean(),
    color="black", linestyle="solid", lw=1, alpha=0,
)
ax.loglog(
    crs_bbh.indep_variable, crs_bbh.mean(),
    color="black", linestyle="solid", lw=1,
)
ax.loglog(
    crs_bns.indep_variable, crs_bns.mean(),
    color="black", linestyle="solid", lw=1,
)
ax.loglog(
    crs_nsbh.indep_variable, crs_nsbh.mean(),
    color="black", linestyle="solid", lw=1,
)

# Label axes
ax.set_xlabel("Primary mass [M$_\\odot$]", fontsize=14)
ax.set_ylabel("Differential Rate [Gpc$^{-3}$ yr$^{-1}$ $M_\\odot^{-1}$]", fontsize=14)

# Label the plot
ax.legend([], [], loc="upper right", title="By Subpopulation", frameon=False)

# Limit the number of orders of magnitude for readability
limit_ax_oom(ax, n_oom, "y")

# Truncate the x-axis
ax.set_xlim(mass_range_min, mass_range_max)

# Set custom tick locations
ax.set_xticks(xaxis_ticks)

# Don't use scientific notation for masses despite the log scale
ax.xaxis.set_major_formatter(major_xaxis_formatter)
ax.xaxis.set_minor_formatter(minor_xaxis_formatter)

# Save to file
fig.savefig("../img/MultiSource_EM-informed_m1_traces_by_subpop.png")


## Legends #####################################################################

# MultiSpin
fig, ax = plt.subplots(figsize=(6,1), constrained_layout=True)

legend_handles = [
    Line2D([0], [0], color=color_bbh, lw=2),
    Line2D([0], [0], color="black", linestyle="solid", lw=2),
    Line2D([0], [0], color=color_bbh_pl, lw=2),
    Line2D([0], [0], color="black", linestyle="dashed", lw=2),
    Line2D([0], [0], color=color_bbh_g, lw=2),
]
legend_labels = [
    "All BBH",
    "PPD",
    "Power law",
    "$\\pm$ 90%",
    "Gaussian",
]
legend = ax.legend(
    legend_handles, legend_labels,
    fancybox=True, shadow=True,
    ncol=3,
)
export_legend(legend, filename="../img/MultiSpin_m1_traces_legend.png")


# MultiSource
fig, ax = plt.subplots(figsize=(6,1), constrained_layout=True)

legend_handles = [
    Line2D([0], [0], color=color_total, lw=2),
    Line2D([0], [0], color=color_bbh, lw=2),
    Line2D([0], [0], color=color_bns, lw=2),
    Line2D([0], [0], color=color_nsbh, lw=2),
    Line2D([0], [0], color="black", linestyle="solid", lw=2),
    Line2D([0], [0], color="black", linestyle="dashed", lw=2),
]
legend_labels = [
    "Overall",
    "BBH",
    "BNS",
    "NSBH",
    "PPD",
    "$\\pm$ 90%",
]
legend = ax.legend(
    legend_handles, legend_labels,
    fancybox=True, shadow=True,
    ncol=3,
)
export_legend(legend, filename="../img/MultiSource_m1_traces_legend.png")
